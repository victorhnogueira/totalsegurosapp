/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabOneScreen from '../screens/TabInicioScreen';
import TabSegurosScreen from '../screens/TabSegurosScreen';
import TabProfileScreen from '../screens/TabProfileScreen';
import TabSobreScreen from '../screens/TabSobreScreen';
import { BottomTabParamList, TabInicioParamList, TabSegurosParamList, TabProfileParamList, TabSobreParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Inicio"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Inicio"
        component={TabInicioNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="home-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Seguros"
        component={TabSegurosNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="newspaper-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Perfil"
        component={TabProfileNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="person-circle-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Sobre"
        component={TabSobreNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="help-circle-outline" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabInicioStack = createStackNavigator<TabInicioParamList>();

function TabInicioNavigator() {
  return (
    <TabInicioStack.Navigator>
      <TabInicioStack.Screen
        name="TabInicioScreen"
        component={TabOneScreen}
        options={{ headerTitle: 'Novidades' }}
      />
    </TabInicioStack.Navigator>
  );
}

const TabSegurosStack = createStackNavigator<TabSegurosParamList>();

function TabSegurosNavigator() {
  return (
    <TabSegurosStack.Navigator>
      <TabSegurosStack.Screen
        name="TabSegurosScreen"
        component={TabSegurosScreen}
        options={{ headerTitle: 'Seguros' }}
      />
    </TabSegurosStack.Navigator>
  );
}

const TabProfileStack = createStackNavigator<TabProfileParamList>();

function TabProfileNavigator() {
  return (
    <TabProfileStack.Navigator>
      <TabProfileStack.Screen
        name="TabProfileScreen"
        component={TabProfileScreen}
        options={{ headerTitle: 'Meu Perfil' }}
      />
    </TabProfileStack.Navigator>
  );
}

const TabSobreStack = createStackNavigator<TabSobreParamList>();

function TabSobreNavigator() {
  return (
    <TabSobreStack.Navigator>
      <TabSobreStack.Screen
        name="TabSobreScreen"
        component={TabSobreScreen}
        options={{ headerTitle: 'Sobre' }}
      />
    </TabSobreStack.Navigator>
  );
}
