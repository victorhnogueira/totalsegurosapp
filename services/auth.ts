import AsyncStorage from '@react-native-async-storage/async-storage'
import jwtDecode, { JwtPayload } from "jwt-decode";

export const TOKEN_KEY = "@totalseguros-Token";
export const getToken = async () => await AsyncStorage.getItem(TOKEN_KEY);
export const isAuthenticated = async () => {
    const token = await getToken()
    if (token === null) {
        return { auth: null }
    }

    const decodedToken = jwtDecode<JwtPayload>(token)
    const currentDate = new Date()

    // JWT exp is in seconds
    //@ts-ignore
    if (!decodedToken || decodedToken?.exp * 1000 < currentDate.getTime()) {
      await logout()
      return { auth: null }
    } else {
      return {
        auth: token
      }
    }

};
export const login = async token => {
    await AsyncStorage.setItem(TOKEN_KEY, token);
};
export const logout = async () => {
    await AsyncStorage.removeItem(TOKEN_KEY);
};