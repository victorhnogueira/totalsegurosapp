/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

export type RootStackParamList = {
  Login: undefined;
  App: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Inicio: undefined;
  Seguros: undefined;
  Perfil: undefined;
  Sobre: undefined;
};

export type TabInicioParamList = {
  TabInicioScreen: undefined;
};

export type TabSegurosParamList = {
  TabSegurosScreen: undefined;
};

export type TabProfileParamList = {
  TabProfileScreen: undefined;
};

export type TabSobreParamList = {
  TabSobreScreen: undefined;
};
