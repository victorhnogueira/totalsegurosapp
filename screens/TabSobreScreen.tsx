import React, { useEffect, useState } from "react";
import { StyleSheet, TouchableOpacity, ScrollView } from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";

export default function TabSobreScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <ScrollView style={{ flex: 1 }}>
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}>
            Total Corretora de seguros
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 16 }}>
            Telefone:
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>(16) 3607-4333</Text>
        </View>
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 16 }}>
            Whatsapp:
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            (16) 9 9600-7830
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 16 }}>
            E-mail
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            comercial@totalcorretoradeseguros.com.br
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 16 }}>
            Endereço
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            Av. Pres. Vargas, 2001 - Sala 27, Jardim Nova Aliança, Ribeirão
            Preto - SP.
          </Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
