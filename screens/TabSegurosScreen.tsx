import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import {
  SafeAreaView,
  StyleSheet,
  Button,
  Linking,
  TouchableOpacity,
  Modal,
  Alert,
  Pressable,
} from "react-native";

import { isAuthenticated, getToken } from "../services/auth";
import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { ScrollView } from "react-native-gesture-handler";

const loadInBrowser = (url) => {
  Linking.openURL(url).catch((err) => console.error("Couldn't load page", err));
};

const SeguroItem = ({ item }) => (
  <TouchableOpacity onPress={() => !!item.cta && loadInBrowser(item.cta)}>
    <View style={styles.seguro}>
      <Text style={styles.seguroTitle}>{item.title}</Text>
      {!!item.content && (
        <Text style={styles.seguroContent}>{item.content}</Text>
      )}
    </View>
  </TouchableOpacity>
);

export default function TabSegurosScreen({ navigation }) {
  const [token, setToken] = useState<string | null>(null);
  const [segurosContratados, setSegurosContratados] = useState<any[]>([]);
  const [seguros, setSeguros] = useState<any[]>([]);

  function loadSegurosAuto() {
    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
    });

    axiosInstance({
      method: "get",
      url: "/client/segurosauto",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        const data = response.data;
        setSegurosContratados(data);
      })
      .catch((error) => {
        setSegurosContratados([]);
      })
      .finally(() => {});
  }

  function loadSeguros() {
    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
    });

    axiosInstance({
      method: "get",
      url: "/seguros"
    })
      .then((response) => {
        const data = response.data;
        setSeguros(data);
      })
      .catch((error) => {
        setSeguros([]);
      })
      .finally(() => {});
  }

  useEffect(() => {
    async function checkAuthenticated() {
      const logged = await isAuthenticated();

      if (logged.auth === null) {
        // deslogado
      } else {
        if (token) {
          loadSegurosAuto();
        }
      }
    }

    checkAuthenticated();
  }, [token]);

  useEffect(() => {
    async function checkData() {
      const data = await getToken();
      setToken(data);
    }
    checkData();
    loadSeguros();
  }, []);

  const SeguroAutoItem = ({ item }) => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
      <Fragment>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
          <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 10 }}>
            <Text
              style={{
                color: "#374151",
                fontWeight: "bold",
                fontSize: 20,
                paddingVertical: 10,
              }}
            >
              Detalhes do Seguro
            </Text>
            <ScrollView style={{ flex: 1 }}>
              <View
                style={{
                  backgroundColor: "#F9FAFB",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Numero da apolice
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.numeroApolice}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Veiculo
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.veiculo}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#F9FAFB",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Placa
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.placa}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Seguradora
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.seguradora}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#F9FAFB",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Central de atendimento
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.centralDeAtendimento}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Franquia
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.franquia}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#F9FAFB",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Danos Materiais
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.danosMateriais}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Danos corporais
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.danosCorporais}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#F9FAFB",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Danos morais
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.danosMorais}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}
                >
                  Carro reserva
                </Text>
                <Text style={{ color: "#374151", fontSize: 14 }}>
                  {item.carroReserva}
                </Text>
              </View>
            </ScrollView>
            <Pressable
              style={{
                backgroundColor: "#6B7280",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 10,
                padding: 10,
              }}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={{ color: "#fff" }}>Fechar</Text>
            </Pressable>
          </View>
        </Modal>
        <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
          <View style={styles.seguro}>
            <Text style={styles.seguroTitle}>
              Apolice: {item.numeroApolice}
            </Text>
            <Text style={styles.seguroSubtitle}>Placa: {item.placa}</Text>
            {!!item.content && (
              <Text style={styles.seguroContent}>{item.content}</Text>
            )}
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 20,
            paddingVertical: 10,
            paddingHorizontal: 15,
            color: "#374151",
          }}
        >
          Seguros contratados
        </Text>
        {!!token && segurosContratados?.length === 0 && (
          <Text
            style={{
              fontSize: 14,
              paddingHorizontal: 20,
              paddingVertical: 40,
              textAlign: 'center',
              color: "#374151",
            }}
          >
            Você não possui nenhum seguro contratado, conheça nossos seguros abaixo.
          </Text>
        )}
        {!token && segurosContratados?.length === 0 && (
          <Fragment>
            <Text
              style={{
                fontSize: 14,
                paddingHorizontal: 20,
                paddingBottom: 5,
                paddingTop:30,
                textAlign: 'center',
                color: "#374151",
              }}
            >
              Faça login para consultar os seguros contratados.
            </Text>
            <TouchableOpacity onPress={() => navigation.replace("Login")}>
              <Text
                style={{
                  fontSize: 14,
                  paddingHorizontal: 20,
                  paddingBottom: 20,
                  textAlign: 'center',
                  color: "#4F46E5",
                }}
              >
                Fazer login
              </Text>
            </TouchableOpacity>
          </Fragment>
        )}
        {segurosContratados?.map((seguro) => (
          <SeguroAutoItem key={seguro.id} item={seguro} />
        ))}
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 20,
            paddingVertical: 10,
            paddingHorizontal: 15,
            color: "#374151",
          }}
        >
          Seguros para você
        </Text>
        {seguros?.map((seguro) => (
          <SeguroItem key={seguro.id} item={seguro} />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  seguro: {
    backgroundColor: "#f9f9f9",
    borderRadius: 5,
    borderColor: "#ccc", // if you need
    borderLeftWidth: 10,
    overflow: "hidden",
    shadowColor: "#000",
    shadowRadius: 10,
    shadowOpacity: 1,
    width: "100%",
    padding: 20,
    marginVertical: 8,
  },
  seguroTitle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#333",
  },
  seguroSubtitle: {
    fontSize: 14,
    color: "#333",
  },
  seguroContent: {
    fontSize: 14,
    textAlign: "justify",
    marginTop: 10,
  },
});
