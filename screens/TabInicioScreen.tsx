import React, { useEffect, useState } from "react";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import axios from "axios";
import {
  FlatList,
  StyleSheet,
  Button,
  Linking,
  Image,
  ScrollView,
} from "react-native";

const loadInBrowser = (url) => {
  Linking.openURL(url).catch((err) => console.error("Couldn't load page", err));
};

const Item = ({ title, content, cta, image }) => (
  <View style={styles.item}>
    <Text style={styles.itemTitle}>{title}</Text>
    {!!image ? (
      <View style={styles.itemImage}>
        <Image style={styles.itemImageStretch} source={{ uri: image }} />
      </View>
    ) : null}
    <Text style={styles.itemContent}>{content}</Text>
    {!!cta ? (
      <View style={{ marginTop: 20 }}>
        <Button
          onPress={() => loadInBrowser(cta)}
          title="Saiba mais"
          color="#059669"
          accessibilityLabel="Saiba mais sobre esse artigo"
        />
      </View>
    ) : null}
  </View>
);

export default function TabInicioScreen({ navigation }) {
  const [posts, setPosts] = useState([]);

  function loadPosts() {
    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
    });

    axiosInstance({
      method: "get",
      url: "/news",
    })
      .then((response) => {
        const data = response.data;
        setPosts(data);
      })
      .catch((error) => {
        setPosts([]);
      })
      .finally(() => {});
  }

  useEffect(() => {
    loadPosts();
  }, []);

  const renderItem = ({ item }) => (
    <Item
      title={item.title}
      content={item.content}
      cta={item.cta}
      image={item.image}
    />
  );

  return (
    <View style={styles.container}>
      {posts.length === 0 ? (
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.item}>
            <View
              style={{ backgroundColor: "#ddd", height: 40, borderRadius: 5 }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 140,
              }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 50,
                borderRadius: 10,
              }}
            ></View>
          </View>
          <View style={styles.item}>
            <View
              style={{ backgroundColor: "#ddd", height: 40, borderRadius: 5 }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 140,
              }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 50,
                borderRadius: 10,
              }}
            ></View>
          </View>
          <View style={styles.item}>
            <View
              style={{ backgroundColor: "#ddd", height: 40, borderRadius: 5 }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 140,
              }}
            ></View>
            <View
              style={{
                backgroundColor: "#ddd",
                marginVertical: 5,
                height: 50,
                borderRadius: 10,
              }}
            ></View>
          </View>
        </ScrollView>
      ) : (
        <FlatList
          style={{ width: "100%" }}
          data={posts}
          renderItem={renderItem}
          keyExtractor={(item) => String(item.id)}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "100%",
  },
  item: {
    backgroundColor: "#f9f9f9",
    borderRadius: 5,
    borderColor: "#f1f1f1", // if you need
    borderWidth: 1,
    overflow: "hidden",
    shadowColor: "#000",
    shadowRadius: 10,
    shadowOpacity: 1,
    width: "100%",
    padding: 20,
    marginVertical: 8,
  },
  itemTitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#333",
  },
  itemContent: {
    fontSize: 14,
    textAlign: "justify",
    marginTop: 10,
  },
  itemImage: {
    width: "100%",
    marginVertical: 10,
  },
  itemImageStretch: {
    width: "100%",
    height: 200,
  },
});
