import { StackScreenProps } from "@react-navigation/stack";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Switch,
} from "react-native";
import { TextInputMask } from 'react-native-masked-text'
import { isAuthenticated, login, logout } from "../services/auth";
import axios from "axios";

import { RootStackParamList } from "../types";

export default function LoginScreen({
  navigation,
}: StackScreenProps<RootStackParamList, "NotFound">) {
  const [credential, setCredential] = useState("");
  const [password, setPassword] = useState("");
  const [serverMessage, setServerMessage] = useState<null | string>();
  const [loadingRequest, setLoadingRequest] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);

  function loginAttempt() {
    setLoadingRequest(true);
    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com/",
    });

    axiosInstance({
      method: "post",
      url: "/auth/client/login",
      data: {
        cpfCnpj: credential,
        password,
      },
    })
      .then((response) => {
        const { token } = response.data;
        setServerMessage(null);
        login(token);
        navigation.replace("App");
      })
      .catch(() => {
        setServerMessage("Informações inválidas");
        logout();
      })
      .finally(() => {
        setLoadingRequest(false);
      });
  }

  useEffect(() => {
    async function checkAuthenticated() {
      const logged = await isAuthenticated();

      if (logged.auth !== null) {
        navigation.replace("App");
      }
    }

    checkAuthenticated();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Total Seguros</Text>
      {isEnabled ? (
        <TextInputMask
          type={'cpf'}
          value={credential}
          onChangeText={text => setCredential(text)}
          placeholder="CPF"
          style={styles.textInputText}
        />
      ) : (
        <TextInputMask
          type={'cnpj'}
          value={credential}
          onChangeText={text => setCredential(text)}
          placeholder="CNPJ"
          style={styles.textInputText}
        />
      )}
      <View style={{ flexDirection: "row", width: "100%" }}>
        <Switch
          trackColor={{ false: "#767577", true: "#767577" }}
          thumbColor={isEnabled ? "#059669" : "#D1D5DB"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={() => setIsEnabled(!isEnabled)}
          value={isEnabled}
        />
        <TouchableOpacity
          onPress={() => setIsEnabled(!isEnabled)}
        >
          <Text>{!isEnabled ? "Entrar com CNPJ" : "Entrar com CPF"}</Text>
        </TouchableOpacity>
      </View>
      <TextInput
        secureTextEntry={true}
        style={[styles.textInputText, { marginTop: 20 }]}
        onChangeText={(text) => setPassword(text)}
        value={password}
        placeholder="Senha"
      />
      <TouchableOpacity
        onPress={() => loginAttempt()}
        style={styles.logginButton}
        disabled={loadingRequest}
      >
        {loadingRequest ? (
          <Text style={{ textAlign: "center", color: "#fff" }}>
            Carregando...
          </Text>
        ) : (
          <Text style={styles.logginButtonText}>LOGIN</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.replace("App")}
        style={styles.continueWithoutLoginButton}
        disabled={loadingRequest}
      >
        <Text style={styles.logginButtonText}>Continuar sem cadastro</Text>
      </TouchableOpacity>
      {!!serverMessage && (
        <Text style={styles.serverMessageText}>{serverMessage}</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  title: {
    fontSize: 40,
    fontWeight: "bold",
    color: "#666",
    marginVertical: 20,
  },
  logginButton: {
    width: "100%",
    marginTop: 15,
    paddingVertical: 15,
    borderRadius: 5,
    backgroundColor: "#43cd7f",
  },
  continueWithoutLoginButton: {
    width: "100%",
    marginTop: 15,
    paddingVertical: 10,
    borderRadius: 5,
    backgroundColor: "#F3F4F6",
    color: "#1F2937",
  },
  logginButtonText: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "#1b5534",
  },
  textInputText: {
    height: 50,
    width: "100%",
    padding: 10,
    marginVertical: 4,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#c1c1c1",
    fontSize: 16,
  },
  serverMessageText: {
    textAlign: "center",
    backgroundColor: "#fd3a4a",
    color: "#fff",
    width: "100%",
    padding: 5,
    marginTop: 10,
    borderRadius: 5,
  },
});
