import React, { useEffect, useState } from "react";
import { StyleSheet, TouchableOpacity, ScrollView } from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { isAuthenticated, logout, getToken } from "../services/auth";
import axios from "axios";

export default function TabInicioScreen({ navigation }) {
  const [token, setToken] = useState<string | null>(null);
  const [client, setClient] = useState<any>(null);

  async function logoutAction() {
    await logout().then(() => {
      navigation.replace("Login");
    });
  }

  function getClientInfo() {
    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
    });

    axiosInstance({
      method: "get",
      url: "/client",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        const data = response.data;
        setClient(data);
      })
      .catch((error) => {
        setClient(null);
      })
  }

  useEffect(() => {
    async function checkAuthenticated() {
      const logged = await isAuthenticated();

      if (logged.auth === null) {
        navigation.replace("Login");
      } else {
        if (token) {
          getClientInfo();
        }
      }
    }

    checkAuthenticated();
  }, [token]);

  useEffect(() => {
    async function checkData() {
      const data = await getToken();
      setToken(data);
    }
    checkData();
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView style={{ flex: 1 }}>
        <View
          style={{
            backgroundColor: "#F9FAFB",
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}>
            Nome completo
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            {client?.fullName}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "#FFFFFF",
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}>
            CPF/CNPJ
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>{client?.cpfCnpj}</Text>
        </View>
        <View
          style={{
            backgroundColor: "#F9FAFB",
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}>
            E-mail
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            {client?.email}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "#FFFFFF",
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20 }}>
            Telefone
          </Text>
          <Text style={{ color: "#374151", fontSize: 14 }}>
            {client?.phone}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "#F9FAFB",
            paddingHorizontal: 10,
            paddingVertical: 5,
          }}
        >
          <Text style={{ color: "#374151", fontWeight: "bold", fontSize: 20, marginBottom: 10 }}>
            Ações
          </Text>
          <TouchableOpacity onPress={() => logoutAction()}>
            <Text
              style={{
                backgroundColor: "#FECACA",
                color: "#991B1B",
                fontWeight: "bold",
                fontSize: 20,
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              sair
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
